import React from "react"

class FormNames extends React.Component {

    constructor(props) {
        super(props)

        this.onAddItem = props.onAddItem
        this.state = {
            nameToAdd: ''
        }
    }

    handleInput = (event) => {
        this.setState({ 
            [event.target.name]: event.target.value
        })        
    }

    onAdd = () => {
        this.onAddItem(this.state.nameToAdd)
        this.setState({ 
            nameToAdd: ''
        })
    }

    render() {
        return (
            <>
                <input type="text" placeholder="Nome do participante"
                    name="nameToAdd" value={this.state.nameToAdd} onChange={this.handleInput} />

                <input type="button" value="+" onClick={this.onAdd} disabled={this.state.nameToAdd === ''} />
            </>
        )
    }

}

export default FormNames