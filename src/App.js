import React from "react";
import FormNames from "./FormNames";
import ListNames from "./ListNames";

class App extends React.Component {

    constructor() {
        super() 
        
        this.state = {
            winner: '',
            listNames: []
        }
    }

    drawWinner = () => {
        let pos = Math.floor(Math.random() * this.state.listNames.length)
        this.setState(currentState => ({
            winner: currentState.listNames[pos]
        }))
    }

    addItemToList = (nameToAdd) => {
        this.setState(currentState => ({ 
            listNames: [...currentState.listNames, nameToAdd]
        }))
    }

    render() {
        return (
            <>
                <FormNames onAddItem={this.addItemToList} />
                <ListNames items={this.state.listNames} />

                <input type="button" value="Sortear vencedor" 
                    onClick={this.drawWinner} disabled={this.state.listNames.length < 3}/> 

                {this.state.winner !== '' && <p>O vencedor é {this.state.winner}</p>}
            </>
        )
    }

}

export default App;
